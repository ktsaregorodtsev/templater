#ifndef DOCUMENTTEMPLATE_H
#define DOCUMENTTEMPLATE_H

#include "abstracttemplate.h"
#include <memory>

// TODO Добавит
class DocumentTemplate : public AbstractTemplate
{
public:
    DocumentTemplate(std::string source);

    std::string beginWord() override;
    std::string endWord() override;
    std::string render(std::string value) override;
private:
    std::list<std::shared_ptr<AbstractTemplate>> m_nodes;
};

#endif // DOCUMENTTEMPLATE_H
