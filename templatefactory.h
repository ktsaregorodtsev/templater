#ifndef TEMPLATEFACTORY_H
#define TEMPLATEFACTORY_H

#include <string>
#include <functional>

class AbstractFilter;
class AbstractTemplate;

using MakeTemplateFunc = std::function<AbstractTemplate *(std::string str, int level)>;
using MakeFilterFunc = std::function<AbstractFilter *(std::string str, int level)>;

void registerTemplate(std::string name, MakeTemplateFunc abstractTemplateFunc);
void registerFilter(std::string name, MakeFilterFunc abstractFilter);

extern const std::string filterDelimeter;
extern const std::string beginControl;
extern const std::string endControl;
extern const bool useSpace;

bool isControlWorld(std::string controlWorld);

class TemplateFactory
{
public:
    TemplateFactory(std::string sourceStr);
    bool next();
    AbstractTemplate *currentTemplate() const;
};

#endif // TEMPLATEFACTORY_H
