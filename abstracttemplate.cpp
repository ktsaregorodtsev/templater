#include "abstracttemplate.h"

#include "abstractfilter.h"
#include "exceptions.h"
#include "templatefactory.h"

#include <iostream>


AbstractTemplate::AbstractTemplate(std::string sourceString, int level) :
    m_sourceStr(sourceString),
    m_currentLevel(level)
{
    if(m_currentLevel >= m_maxLevel)
        throw MaxDepthExceptions();
}

void AbstractTemplate::addFilter(AbstractFilter *filter)
{
    m_filters.push_back(filter);
}

std::string AbstractTemplate::filtered(std::string value)
{
    std::string accum = render(value);
    for(auto * filter: m_filters)
        accum = (*filter)(accum);
    return accum;
}

void AbstractTemplate::process()
{
    int beginControlLen = beginControl.length();
    int endControlLen = endControl.length();
    while(m_sourceStr.length() != 0) {
        if(m_sourceStr.substr(0, beginControlLen) == beginControl) {
            // TODO Space items
            int pos = m_sourceStr.find(endControl, beginControlLen);
            if(pos == -1) throw ErrorTagExceptions();
            std::cout << ":::" << m_sourceStr.substr(0, pos + endControlLen) << std::endl;
            m_sourceStr = m_sourceStr.substr(pos + endControlLen);
        }
        else {
            m_renderStr += m_sourceStr;
            m_sourceStr = m_sourceStr.substr(1);
        }
    }
    std::cout << "m_renderStr: " << m_renderStr << std::endl
              << "m_sourceStr: " << m_sourceStr << std::endl;
}
