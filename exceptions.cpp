#include "exceptions.h"


const char *MaxDepthExceptions::what() const throw()
{
    return "Depth out of range";
}

const char *ErrorTagExceptions::what() const throw()
{
    return  "Error tag";
}
