#include "documenttemplate.h"

DocumentTemplate::DocumentTemplate(std::string source) :
    AbstractTemplate(source, 0)
{

}

std::string DocumentTemplate::beginWord()
{
    return "";
}

std::string DocumentTemplate::endWord()
{
    return "";
}

std::string DocumentTemplate::render(std::string /* value */)
{
    process();
    return "";
}
