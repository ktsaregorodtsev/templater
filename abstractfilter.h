#ifndef ABSTRACTFILTER_H
#define ABSTRACTFILTER_H

#include <string>


class AbstractFilter
{
public:
    std::string operator() (std::string parameter);
    virtual std::string name() const = 0;

protected:
    virtual std::string filter(std::string parameter) = 0;

};

#endif // ABSTRACTFILTER_H
