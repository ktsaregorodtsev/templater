#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>

class MaxDepthExceptions : public std::exception
{
public:
    virtual const char *what() const throw();
};

class ErrorTagExceptions : public std::exception
{
public:
    virtual const char *what() const throw();
};


#endif // EXCEPTIONS_H
