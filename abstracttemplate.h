#ifndef ABSTRACTTEMPLATE_H
#define ABSTRACTTEMPLATE_H

#include <string>
#include <list>

class AbstractFilter;

class AbstractTemplate
{
public:
    AbstractTemplate(std::string sourceString, int level);
    virtual std::string beginWord() = 0;
    virtual std::string endWord() = 0;

    void addFilter(AbstractFilter *filter);
    std::string filtered(std::string value) ;

protected:
    virtual std::string render(std::string value) = 0;
    void process();

private:
    std::list<AbstractFilter *> m_filters;
    std::string m_sourceStr;
    std::string m_renderStr = "";
    int m_currentLevel = 0;
    int m_maxLevel = 50;



};

#endif // ABSTRACTTEMPLATE_H
