#include "templatefactory.h"

#include "abstractfilter.h"
#include "abstractfilter.h"

#include  <map>
#include <algorithm>

const std::string filterDelimeter = "%#";
const std::string beginControl = "%%";
const std::string endControl = "%%";
const bool useSpace = false;

using TemplateFuncMap = std::map<std::string, MakeTemplateFunc>;
using FilterFuncMap = std::map<std::string, MakeFilterFunc>;

TemplateFuncMap m_templates;
FilterFuncMap m_filters;

void registerTemplate(std::string name, MakeTemplateFunc templateFunc)
{
    m_templates[name] = templateFunc;
}

void registerFilter(std::string name, MakeFilterFunc abstractFilter)
{
    m_filters[name] = abstractFilter;
}

TemplateFactory::TemplateFactory(std::string sourceStr)
{

}

bool isControlWorld(std::string controlWorld)
{
    return std::find_if(m_templates.begin(), m_templates.end(),
                        [controlWorld](TemplateFuncMap::value_type v){
        return v.first == controlWorld; }) == m_templates.end();
}
